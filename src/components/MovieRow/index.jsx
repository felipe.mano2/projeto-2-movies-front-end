import React from 'react'
import Badge from '../../components/Badge'
import Slider from '../Slider'
import { MovieRow } from './MovieRowElements'

const MovieRowContainer = ({ title, color, movies, handleClick }) => {
  return (
    <MovieRow>
      <Badge title={title} color={color} />
      <Slider movies={movies} handleClick={handleClick} />
    </MovieRow>
  )
}

export default MovieRowContainer
