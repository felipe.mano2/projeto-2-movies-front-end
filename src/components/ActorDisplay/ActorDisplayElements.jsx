import { Card } from 'reactstrap'
import styled from 'styled-components'

export const ActorCard = styled(Card)`
  height: 100%;
  width: 225px;
  margin: 0 2px;
  transition: transform 300ms ease 100ms;
`

export const ActorImage = styled.img`
  width: 200px;
  height: 325px;
`
