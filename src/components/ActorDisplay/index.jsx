import React from 'react'
import { CardBody, CardTitle, CardSubtitle } from 'reactstrap'
import { ActorCard, ActorImage } from './ActorDisplayElements'

const Actor = ({
  member: { profile_path, name, character, known_for_department },
}) => {
  return (
    <ActorCard>
      <ActorImage
        src={
          profile_path
            ? `https://image.tmdb.org/t/p/original/${profile_path}`
            : 'https://sd.keepcalms.com/i/keep-calm-poster-not-found.png'
        }
        alt={name}
      />
      <CardBody>
        <CardTitle tag='h5'>{name}</CardTitle>
        <CardSubtitle tag='h6' className='mb-2 text-muted'>
          As {character}
        </CardSubtitle>
      </CardBody>
      <CardBody></CardBody>
    </ActorCard>
  )
}

export default Actor
