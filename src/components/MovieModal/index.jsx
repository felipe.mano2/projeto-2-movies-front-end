import React from 'react'
import ActorDisplay from '../ActorDisplay'
import {
  CloseButton,
  RentButton,
  ModalContainer,
  ModalH,
  ModalB,
  ModalF,
  PosterImage,
  Box,
  ImageContainer,
  VideoContainer,
  ModalTitle,
  Video,
  CustomBox,
  Paragraph,
} from './MovieModalElements'
import Badge from '../Badge'
import Slider from '../Slider'

const MovieModal = props => {
  const {
    modal,
    modalIsOpen,
    movie: {
      title,
      genres,
      overview,
      poster_path,
      release_date,
      vote_average,
      tagline,
      directors,
      credits,
      videos,
    },
    genresColors,
  } = props

  return (
    <ModalContainer size='xl' isOpen={modal} centered>
      <ModalH>
        {genres?.map(genre => {
          return (
            <Badge
              title={genre.name}
              color={genresColors.find(g => g.name === genre.name)?.color}
              key={genre.id}
            />
          )
        })}
      </ModalH>
      <ModalB>
        <ImageContainer>
          <PosterImage
            src={
              poster_path
                ? `https://image.tmdb.org/t/p/original/${poster_path}`
                : 'https://sd.keepcalms.com/i/keep-calm-poster-not-found.png'
            }
            alt={title}
            className='img-fluid'
          />
          <VideoContainer>
            <Video
              id='ytplayer'
              width='960'
              height='540'
              src={`https://www.youtube.com/embed/${videos?.results[0]?.key}?autoplay=1`}
            />
            <Box>
              <ModalTitle>
                <h1>{title}</h1>
                <h2>★{vote_average}</h2>
              </ModalTitle>
              <p>{tagline}</p>
              <h3>Release Date</h3>
              <Paragraph>{release_date}</Paragraph>
            </Box>
          </VideoContainer>
        </ImageContainer>
        <Box>
          <Box>
            <h3>Overview</h3>
            <Paragraph>{overview ? overview : title}</Paragraph>
            <CustomBox>
              <Box>
                <h3>Director</h3>
                <Paragraph>
                  {directors?.length > 0 ? directors[0] : '-'}
                </Paragraph>
              </Box>
              <RentButton color='primary' onClick={modalIsOpen}>
                Watch Now
              </RentButton>
              <CloseButton color='secondary' onClick={modalIsOpen}>
                Close
              </CloseButton>
            </CustomBox>
          </Box>
        </Box>
      </ModalB>
      <ModalF></ModalF>
      <Badge title='Actors' color='#000'></Badge>
      <Slider cast={credits}>
        {credits?.cast?.map(member => (
          <ActorDisplay key={member.id} member={member} />
        ))}
      </Slider>
    </ModalContainer>
  )
}

export default MovieModal
