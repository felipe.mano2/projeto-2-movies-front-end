import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap'
import styled from 'styled-components'

export const CloseButton = styled(Button)`
  margin: 3em 0.5em;
  padding: 2%;
`

export const RentButton = styled(Button)`
  margin: 3em 0em;
  padding: 2%;
  flex: 1;
`

export const CustomBox = styled.div`
  display: flex;
  justify-content: space-between;
`

export const Paragraph = styled.div`
  font-size: 1.2em;
`

export const ButtonsContainer = styled.div``

export const ModalContainer = styled(Modal)`
  max-width: 70%;

  .modal-content {
  }
`

export const ModalH = styled.div`
  justify-content: center;
  display: flex;
  background-color: #2e282a;
`

export const ModalB = styled(ModalBody)`
  /* display: flex; */
`

export const ModalF = styled(ModalFooter)``

export const PosterImage = styled.img`
  max-width: 500px;
`

export const ImageContainer = styled.div`
  display: flex;
`

export const Box = styled.div`
  flex: 1;
  margin: 10px 0px;
`

export const CastingContainer = styled.div`
  display: flex;
`

export const Video = styled.iframe`
  width: 100%;
`
export const VideoContainer = styled.div`
  width: 100%;
  margin: 0 0 0 15px;
`

export const ModalTitle = styled.div`
  display: flex;
  justify-content: space-between;
`
