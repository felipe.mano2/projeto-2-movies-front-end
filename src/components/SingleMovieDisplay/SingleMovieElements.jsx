import styled from 'styled-components'
import { Card, CardBody, CardText, CardImg, CardTitle } from 'reactstrap'

export const CardImage = styled(CardImg)`
  width: 400px;
`

export const MovieCard = styled(Card)`
  margin: 0em 0.1em;
  min-width: 300px;
  height: 450px;
  cursor: pointer;
  :hover {
    /* transform: scale(1.5); */
    /* transform: translateX(-25%);
    transform: translateX(25%); */
  }
`
export const MovieCardBody = styled(CardBody)`
  padding: 2px;
  text-align: left;
  display: flex;
  flex-direction: column;
`

export const CardFooter = styled.div`
  display: flex;
  justify-content: flex-end;
`

export const CardComment = styled(CardText)`
  flex: 1;
`

export const PriceTag = styled.div`
  margin-right: 5px;
  font-size: 1.5em;
`

export const DurationTag = styled.div`
  font-size: 1.5em;
`

export const CardTitled = styled(CardTitle)``
