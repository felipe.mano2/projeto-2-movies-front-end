import React, { useEffect, useState } from 'react'
import {
  CardImage,
  MovieCard,
  CardFooter,
  CardComment,
  PriceTag,
  DurationTag,
  MovieCardBody,
  CardTitled,
} from './SingleMovieElements'
import { MdAttachMoney } from 'react-icons/md'
import { BiTime } from 'react-icons/bi'

const SingleMovieDisplay = ({ handleClick, movie }) => {
  const {
    original_title,
    overview,
    popularity,
    backdrop_path,
    id,
    original_name,
  } = movie

  const [duration, setDuration] = useState(null)

  useEffect(() => {
    setDuration(Math.floor(Math.random() * (160 - 70 + 1) + 70))
  }, [])

  return (
    <MovieCard id={id}>
      <CardImage
        src={`https://image.tmdb.org/t/p/w500/${backdrop_path}`}
        alt='Card image cap'
        onClick={e => handleClick(e)}
        id={id}
      />
      <MovieCardBody onClick={e => handleClick(e)} id={original_title}>
        <CardTitled tag='h4' className='' onClick={e => handleClick(e)} id={id}>
          {original_title ? original_title : original_name}
        </CardTitled>
        <CardComment onClick={e => handleClick(e)} id={id}>
          {overview.length > 150
            ? `${overview.substring(0, 150 + 1)}...`
            : overview}
        </CardComment>
        <CardFooter onClick={e => handleClick(e)} id={id}>
          <PriceTag>
            <MdAttachMoney />
            {popularity > 1500 ? 4.99 : 3.99}
          </PriceTag>
          <DurationTag>
            <BiTime />
            {duration}min
          </DurationTag>
        </CardFooter>
      </MovieCardBody>
    </MovieCard>
  )
}

export default SingleMovieDisplay
