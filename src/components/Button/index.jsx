import React from 'react'
import styled from 'styled-components'

const ButtonContainer = ({ children, id }) => (
  <Button id={id}>{children}</Button>
)

export default ButtonContainer

const Button = styled.button`
  display: block;
  margin: 1em 0 0;
  padding: 0.65em 1em 1em;
  background-color: ${props => props.theme.primary};
  border: none;
  border-radius: 4px;
  box-sizing: border-box;
  box-shadow: none;
  width: 100%;
  height: 65px;
  font-size: 1.55em;
  color: ${props => props.theme.fontColor};
  font-weight: 600;
  font-family: inherit;
  transition: background-color 0.2s ease-out;
  &:hover,
  &:active {
    background-color: ${props => props.theme.support};
  }
`
