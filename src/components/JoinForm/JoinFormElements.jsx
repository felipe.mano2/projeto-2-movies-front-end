import { Field } from 'formik'
import styled from 'styled-components'

export const Join = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 80%;
  background-color: #fff;
  margin: 0;
  padding: 2.25em;
  box-sizing: border-box;
  border: solid 2px ${props => props.theme.primary};
  border-radius: 0.5em;

  .select {
    margin: 0;
    padding: 0 0 0 5px;
    background-color: white;
    border: solid 2px black;
    border-radius: 4px;
    -webkit-appearance: none;
    width: 100%;
    height: 65px;
    font-size: 1.55em;
    color: #353538;
    font-weight: 600;
    font-family: inherit;
    transition: box-shadow 0.2s linear, border-color 0.25s ease-out;
    &:focus {
      outline: none;
      box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
      border: solid 2px ${props => props.theme.yellow};
    }
  }
`

export const Input = styled(Field)`
  /* display: block; */
  margin: 0;
  padding: 0 0 0 5px;
  background-color: white;
  border: solid 2px black;
  border-radius: 4px;
  -webkit-appearance: none;
  width: 100%;
  /* max-width: 350px; */
  height: 65px;
  font-size: 1.4em;
  color: #353538;
  font-weight: 600;
  font-family: inherit;
  transition: box-shadow 0.2s linear, border-color 0.25s ease-out;
  &:focus {
    outline: none;
    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
    border: solid 2px ${props => props.theme.yellow};
  }
`

export const Box = styled.div`
  min-width: 250px;
`

export const MainSection = styled.div``

export const AddressSection = styled.div`
  flex: 1;
  align-self: center;
`

export const FlexBox = styled.div`
  display: flex;
`

export const Select = styled(Field)`
  margin: 0;
  padding: 0 0 0 5px;
  background-color: white;
  border: solid 2px black;
  border-radius: 4px;
  width: 100%;
  height: 65px;
  font-size: 1.55em;
  color: #353538;
  text-align: center;

  &:focus {
    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
    border: solid 2px ${props => props.theme.yellow};
  }
`

export const VerticalLine = styled.div`
  border-left: thick solid #000;
  margin: 0 2em;
`

export const Number = styled(FlexBox)`
  align-self: center;
`
