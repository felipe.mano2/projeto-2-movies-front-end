import React from 'react'
import Label from '../Label'
import Button from '../Button'
import { Input, Number, VerticalLine } from './JoinFormElements'
import {
  Join,
  Box,
  FlexBox,
  AddressSection,
  MainSection,
} from './JoinFormElements'
import { Field, Form, Formik } from 'formik'

const JoinContainer = () => {
  return (
    <Join>
      <Formik
        initialValues={{
          cpf: '',
          fullName: '',
          phone: '',
          email: '',
          birthday: '',
          username: '',
          password: '',
          cep: '',
          street: '',
          number: '',
          complement: '',
        }}
        onSubmit={values => {
          alert(JSON.stringify(values, null, 2))
        }}
      >
        <Form>
          <Box>
            <h1>Be a member</h1>
            <p>And start renting movies right away!</p>
          </Box>
          <FlexBox>
            <MainSection className='container'>
              <Box className='row'>
                <Box className='col-md'>
                  <Label htmlFor='cpf'>CPF</Label>
                  <Input
                    id='cpf'
                    name='cpf'
                    type='text'
                    placeholder='000.000.000-00'
                  />
                </Box>
                <Box className='col-md'>
                  <Label htmlFor='fullName'>Full Name</Label>
                  <Input
                    id='fullName'
                    name='fullName'
                    type='text'
                    placeholder='Name Surname'
                  />
                </Box>
              </Box>
              <Box className='row'>
                <Box className='col-md'>
                  <Label htmlFor='phone'>Phone</Label>
                  <Input
                    id='phone'
                    name='phone'
                    type='text'
                    placeholder='(00)00000-0000'
                  />
                </Box>
                <Box className='col-md'>
                  <Label htmlFor='email'>Email Address</Label>
                  <Input
                    id='email'
                    name='email'
                    type='email'
                    placeholder='email@domain.com'
                  />
                </Box>
              </Box>
              <Box className='row'>
                <Box className='col-md'>
                  <Label htmlFor='birthday'>Birthday</Label>
                  <Input id='birthday' name='birthday' type='date' />
                </Box>
                <Box className='col-md'>
                  <Label htmlFor='gender'>Gender</Label>
                  <Field
                    as='select'
                    id='gender'
                    name='gender'
                    className='select'
                  >
                    <option value='0'>---</option>
                    <option value='1'>Male</option>
                    <option value='2'>Female</option>
                    <option value='3'>Other</option>
                  </Field>
                </Box>
              </Box>
              <Box className='row'>
                <Box className='col-sm'>
                  <Label htmlFor='username'>Username</Label>
                  <Input id='username' name='username' type='text' />
                </Box>
                <Box className='col-sm'>
                  <Label htmlFor='password'>Password</Label>
                  <Input id='password' name='password' type='password' />
                </Box>
              </Box>
            </MainSection>
            <VerticalLine />
            <AddressSection className='container'>
              <Box className='row'>
                <Box className='col-'>
                  <Label htmlFor='cep'>CEP</Label>
                  <Input
                    id='cep'
                    name='cep'
                    type='text'
                    placeholder='00000-000'
                  />
                </Box>
              </Box>
              <Box className='col-sm'>
                <Label htmlFor='street'>Street</Label>
                <Input id='street' name='street' type='text' />
              </Box>
              <Box className='col-sm'>
                <Label htmlFor='complement'>Complement</Label>
                <Input id='complement' name='complement' type='text' />
              </Box>
            </AddressSection>
            <Number>
              <Box className=''>
                <Label htmlFor='number'>Number</Label>
                <Input id='number' name='number' type='text' />
              </Box>
            </Number>
          </FlexBox>
          <Button type='submit'>Submit</Button>
        </Form>
      </Formik>
    </Join>
  )
}

export default JoinContainer
