import React from 'react'
import { IoIosArrowForward, IoIosArrowBack } from 'react-icons/io'
import { VisibilityContext, ScrollMenu } from 'react-horizontal-scrolling-menu'
import styled from 'styled-components'

export const LeftArrow = () => {
  const { isFirstItemVisible, scrollPrev } = React.useContext(VisibilityContext)

  return (
    <BackArrow disabled={isFirstItemVisible} onClick={() => scrollPrev()}>
      Left
    </BackArrow>
  )
}

export const RightArrow = () => {
  const { isLastItemVisible, scrollNext } = React.useContext(VisibilityContext)

  return (
    <ForwardArrow disabled={isLastItemVisible} onClick={() => scrollNext()}>
      Right
    </ForwardArrow>
  )
}

export const MenuScroll = styled(ScrollMenu)`
  justify-content: center;
  width: 100%;
`

const ForwardArrow = styled(IoIosArrowForward)`
  font-size: 100px;
  align-self: center;
  cursor: pointer;
`

const BackArrow = styled(IoIosArrowBack)`
  font-size: 100px;
  align-self: center;
  cursor: pointer;
`
