import React from 'react'
import SingleMovieDisplay from '../SingleMovieDisplay'
import { RightArrow, LeftArrow, MenuScroll } from './SliderElements'
import ActorDisplay from '../ActorDisplay'
const Slider = ({ movies, handleClick, cast }) => {
  return (
    <MenuScroll LeftArrow={LeftArrow} RightArrow={RightArrow}>
      {movies?.map(movie => (
        <SingleMovieDisplay
          movie={movie}
          itemId={movie.id}
          key={movie.id}
          handleClick={handleClick}
        />
      ))}
      {cast?.cast?.map(member => (
        <ActorDisplay
          member={member}
          itemId={member.id}
          key={member.id}
          handleClick={handleClick}
        />
      ))}
    </MenuScroll>
  )
}

export default Slider
