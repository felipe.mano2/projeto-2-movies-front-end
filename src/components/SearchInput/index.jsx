import React from 'react'
import { Input } from 'reactstrap'
import styled from 'styled-components'
import { GoSearch } from 'react-icons/go'

const SearchInputContainer = () => {
  return (
    <>
      <SearchBox>
        <Icon>
          <GoSearch />
        </Icon>
        <SearchInput placeholder='Search' />
      </SearchBox>
    </>
  )
}

export default SearchInputContainer

const SearchBox = styled.div`
  display: flex;
  background-color: white;
  border-radius: 2em;
  width: 60%;
  margin: 2em auto;
  border: 1px solid #ccc;
`
const SearchInput = styled(Input)`
  font-size: 1.5em;
  padding: 0.5em;
  width: 100%;
  border-radius: 2em;
  border: none;
  &:focus {
    box-shadow: inset 0 -1px 0 #ddd;
  }
`

const Icon = styled.div`
  align-self: center;
  font-size: 1.5em;
  margin: 0.25em;
`
