import React from 'react'
import styled from 'styled-components'

const LabelContainer = ({ htmlFor, id, children }) => {
  return (
    <Label htmlFor={htmlFor} id={id}>
      {children}
    </Label>
  )
}

export default LabelContainer

const Label = styled.label`
  margin: 0 0 12px;
  display: block;
  font-size: 1.25em;
  color: ${props => props.theme.pastel};
  font-weight: 700;
  font-family: inherit;
`
