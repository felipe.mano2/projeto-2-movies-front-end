import { Link } from '@reach/router'
import { Formik } from 'formik'
import React from 'react'
import Button from '../Button'
import Label from '../Label'
import { Login, Input } from './LoginFormElements'

const LoginContainer = ({ handleClick, isPasswordVisible }) => {
  return (
    <Formik
      initialValues={{
        email: '',
        password: '',
        show: '',
      }}
      onSubmit={values => {
        alert(JSON.stringify(values, null, 2))
      }}
    >
      <Login>
        <div className='svgContainer'>
          <div>
            <img
              className='mySVG'
              src='/img/Logo-responsive.png'
              alt='Logo'
            ></img>
          </div>
        </div>
        <div className='inputGroup inputGroup1'>
          <Label htmlFor='loginEmail' id='loginEmailLabel'>
            Email
          </Label>
          <Input
            type='email'
            id='loginEmail'
            maxLength='254'
            name='email'
            placeholder='email@domain.com'
          />
        </div>
        <div className='inputGroup inputGroup2'>
          <Label htmlFor='loginPassword' id='loginPasswordLabel'>
            Password
          </Label>
          <Input
            type={isPasswordVisible ? 'text' : 'password'}
            id='loginPassword'
            name='password'
          />
          <Label id='showPasswordToggle' htmlFor='showPasswordCheck'>
            Show
            <Input
              id='showPasswordCheck'
              type='checkbox'
              onClick={handleClick}
              name='show'
            />
            <div className='indicator'></div>
          </Label>
        </div>
        <div className='inputGroup inputGroup3'>
          <Button id='login'>Log in</Button>
        </div>
        <div>
          Not registered? <Link to='/join'> Click here.</Link>
        </div>
      </Login>
    </Formik>
  )
}

export default LoginContainer
