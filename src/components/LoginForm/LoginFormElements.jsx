import styled from 'styled-components'
import { Field } from 'formik'

export const Login = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: block;
  width: 100%;
  max-width: 400px;
  background-color: #fff;
  margin: 0;
  padding: 2.25em;
  box-sizing: border-box;
  border: solid 1px ${props => props.theme.primary};
  border-radius: 0.5em;

  .svgContainer {
    position: relative;
    width: 200px;
    height: 200px;
    margin: 0 auto 1em;
    border-radius: 50%;
    pointer-events: none;
    div {
      position: relative;
      width: 100%;
      height: 0;
      overflow: hidden;
      border-radius: 50%;
      padding-bottom: 100%;
    }
    .mySVG {
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
    }
    &:after {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      z-index: 10;
      width: inherit;
      height: inherit;
      box-sizing: border-box;
      border: solid 2.5px ${props => props.theme.primary};
      border-radius: 50%;
    }
  }
  .inputGroup {
    margin: 0 0 2em;
    padding: 0;
    position: relative;
    &:last-of-type {
      margin-bottom: 0;
    }
  }

  .inputGroup1 {
    .helper {
      position: absolute;
      z-index: 1;
      font-family: inherit;
    }
    .helper1 {
      top: 0;
      left: 0;
      transform: translate(1em, 2.2em) scale(1);
      transform-origin: 0 0;
      color: ${props => props.theme.support};
      font-size: 1.55em;
      font-weight: 400;
      opacity: 0.65;
      pointer-events: none;
      transition: transform 0.2s ease-out, opacity 0.2s linear;
    }

    &.focusWithText .helper {
      transform: translate(1em, 1.55em) scale(0.6);
      opacity: 1;
    }
  }
  .inputGroup2 {
    #showPasswordToggle {
      display: block;
      padding: 0 0 0 1.45em;
      position: absolute;
      top: 0.25em;
      right: 0;
      font-size: 1em;
      input {
        position: absolute;
        z-index: -1;
        opacity: 0;
      }
      .indicator {
        position: absolute;
        top: 0;
        left: 0;
        height: 0.85em;
        width: 0.85em;
        border: solid 2px ${props => props.theme.support};
        border-radius: 3px;
        &:after {
          content: '';
          position: absolute;
          left: 0.25em;
          top: 0.025em;
          width: 0.2em;
          height: 0.5em;
          background-color: ${props => props.theme.primary};
          border: solid ${props => props.theme.support};
          border-width: 0 3px 3px 0;
          transform: rotate(45deg);
          visibility: hidden;
        }
      }
      input:checked ~ .indicator {
        &:after {
          visibility: visible;
        }
      }
    }
  }
`
export const Input = styled(Field)`
  /* display: block; */
  margin: 0;
  padding: 0 0 0 5px;
  background-color: white;
  border: solid 2px black;
  border-radius: 4px;
  -webkit-appearance: none;
  width: 100%;
  /* max-width: 350px; */
  height: 65px;
  font-size: 1.4em;
  color: #353538;
  font-weight: 600;
  font-family: inherit;
  transition: box-shadow 0.2s linear, border-color 0.25s ease-out;
  &:focus {
    outline: none;
    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
    border: solid 2px ${props => props.theme.yellow};
  }
`
