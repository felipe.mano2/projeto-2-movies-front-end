import { Link } from '@reach/router'
import styled from 'styled-components'

export const Nav = styled.nav`
  background: ${props => props.theme.primary};
  display: flex;
  align-items: center;
  padding: 10px;
  align-items: flex-start;

  @media screen and (max-width: 768px) {
    transition: 0.8s all ease;
  }
`

export const LogoContainer = styled.div`
  flex: 1;
`

export const NavLogo = styled(Link)``

export const Logo = styled.img`
  @media screen and (max-width: 860px) {
    width: 450px;
  }
`

export const NavLink = styled(Link)`
  cursor: pointer;
  color: ${props => props.theme.fontColor};
  align-items: center;
  text-decoration: none;
  font-size: 1em;
  margin: 0 3rem;
  height: 100%;
  align-self: center;

  &:hover {
    color: ${props => props.theme.yellow};
    text-decoration: none;
  }
`

export const MobileIcon = styled.div`
  display: none;

  @media screen and (max-width: 860px) {
    display: block;
    position: absolute;
    font-size: 1.8rem;
    top: 0;
    right: 0;
    transform: translate(-100%, 60%);
    cursor: pointer;
    color: ${props => props.theme.fontColor};
  }
`

export const NavMenu = styled.ul`
  display: flex;
  list-style: none;
  text-align: center;
  margin-bottom: 0;

  @media screen and (max-width: 860px) {
    display: none;
  }
`

export const NavItem = styled.li`
  font-size: 1.5em;
`
