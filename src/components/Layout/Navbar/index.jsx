import React from 'react'
import {
  Nav,
  NavLogo,
  NavLink,
  MobileIcon,
  NavMenu,
  NavItem,
  LogoContainer,
  Logo,
} from './NavbarElements'
import { FaBars } from 'react-icons/fa'

const Navbar = ({ toggle }) => {
  return (
    <Nav>
      <LogoContainer>
        <NavLogo to='/'>
          <Logo
            src='/img/Logo-big.png'
            alt='Logo Flixbuster'
            className='img-fluid'
          />
        </NavLogo>
      </LogoContainer>
      <MobileIcon onClick={toggle}>
        <FaBars />
      </MobileIcon>
      <NavMenu>
        <NavItem>
          <NavLink to='/'>Home</NavLink> |
        </NavItem>
        <NavItem>
          <NavLink to='/movies'>Movies</NavLink> |
        </NavItem>
        <NavItem>
          <NavLink to='/join'>Join</NavLink> |
        </NavItem>
        <NavItem>
          <NavLink to='/user/dashboard'>Dashboard</NavLink>
        </NavItem>
      </NavMenu>
    </Nav>
  )
}

export default Navbar
