import styled from 'styled-components'

export const Footer = styled.div`
  padding: 2em;
  font-size: 1.5em;
  color: ${props => props.theme.fontColor};
  background-color: ${props => props.theme.primary};
  text-align: center;
  font-weight: 600;
`
