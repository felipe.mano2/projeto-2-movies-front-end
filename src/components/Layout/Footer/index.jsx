import React from 'react'
import { Footer } from './FooterElements'

const FooterContainer = () => {
  return <Footer> © FLIXBUSTER | ALL RIGHTS RESERVED</Footer>
}

export default FooterContainer
