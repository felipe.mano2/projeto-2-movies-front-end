import React from 'react'
import Navbar from '../Navbar'
import Topbar from '../Topbar'

const Header = ({ isOpen, toggle }) => (
  <>
    <Topbar isOpen={isOpen} toggle={toggle} />
    <Navbar toggle={toggle} />
  </>
)

export default Header
