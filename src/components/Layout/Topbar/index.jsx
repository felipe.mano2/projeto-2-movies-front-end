import React from 'react'
import {
  TopbarContainer,
  Icon,
  CloseIcon,
  TopbarWrapper,
  TopbarMenu,
  TopbarLink,
  TopBtnWrapper,
  TopbarRoute,
} from './TopbarElements'

const Topbar = ({ isOpen, toggle }) => {
  return (
    <TopbarContainer isOpen={isOpen} onClick={toggle}>
      <Icon onClick={toggle}>
        <CloseIcon />
      </Icon>
      <TopbarWrapper>
        <TopbarMenu>
          <TopbarLink to='/' onClick={toggle}>
            Home
          </TopbarLink>
          <TopbarLink to='/movies' onClick={toggle}>
            Movies
          </TopbarLink>
          <TopbarLink to='/join' onClick={toggle}>
            Join
          </TopbarLink>
        </TopbarMenu>
        <TopBtnWrapper>
          <TopbarRoute to='/user/dashboard' onClick={toggle}>
            Dashboard
          </TopbarRoute>
        </TopBtnWrapper>
      </TopbarWrapper>
    </TopbarContainer>
  )
}

export default Topbar
