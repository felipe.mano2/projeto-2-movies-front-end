import React, { useState } from 'react'
import Header from './Header/'
import Footer from './Footer/'
import styled from 'styled-components'

const LayoutContainer = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false)

  const toggle = () => setIsOpen(!isOpen)

  return (
    <Layout>
      <Header toggle={toggle} isOpen={isOpen} />
      <Main>{children}</Main>
      <Footer />
    </Layout>
  )
}

export default LayoutContainer

const Main = styled.main`
  flex: 1;
`

const Layout = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`
