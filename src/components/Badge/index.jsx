import React from 'react'
import { CustomBadge, BadgeDiv } from './BadgeElements'

const BadgeContainer = ({ title, color }) => {
  return (
    <BadgeDiv>
      <CustomBadge color={color} title={title}>
        {title}
      </CustomBadge>
    </BadgeDiv>
  )
}

export default BadgeContainer
