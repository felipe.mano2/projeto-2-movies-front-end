import { Badge } from 'reactstrap'
import styled from 'styled-components'

export const CustomBadge = styled(Badge)`
  background: ${props => (props.color ? props.color : '#0081AF')};
  width: 250px;
  padding: 1.2em;
  margin: 1.5em 0.2em;
`
export const BadgeDiv = styled.div`
  display: flex;
  justify-content: flex-start;
`
