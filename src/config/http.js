import axios from 'axios'

export const moviesEndpoint = 'movies'

export const http = axios.create({
  baseURL: 'http://localhost:3004'
})