import React from 'react'
import ReactDOM from 'react-dom'
import Routers from './Routers'
import GlobalStyle from './assets/globalStyled'
import 'bootstrap/dist/css/bootstrap.min.css'
import { ThemeProvider } from 'styled-components'
import { mainTheme } from './assets/themes'

ReactDOM.render(
  <React.Fragment>
    <ThemeProvider theme={mainTheme}>
      <Routers />
      <GlobalStyle />
    </ThemeProvider>
  </React.Fragment>,
  document.getElementById('root')
)
