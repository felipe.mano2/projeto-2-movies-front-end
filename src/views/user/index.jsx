import React from 'react'
import { Router } from '@reach/router'
import Dashboard from './Dashboard'

const User = () => (
  <Router>
    <Dashboard path='/dashboard' />
  </Router>
)

export default User
