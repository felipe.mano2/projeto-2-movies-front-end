import { Redirect } from '@reach/router'
import React from 'react'

const DashboardContainer = () => {
  const isUserAuthenticated = false

  return (
    <>
      {!isUserAuthenticated && <Redirect to='/login' />}
      <h1>Dashboard, usuário autenticado</h1>
    </>
  )
}

export default DashboardContainer
