import React, { useState, useEffect } from 'react'
import { Container } from 'reactstrap'
import styled from 'styled-components'
import SearchInput from '../../components/SearchInput'
import MovieRow from '../../components/MovieRow'
import { getTMDB, getAllTMDB } from '../../services/movies.service'
import MovieModal from '../../components/MovieModal'

const MoviesContainer = () => {
  // Movies data
  const [movies, setMovies] = useState([])
  const [singleMovie, setSingleMovie] = useState([])
  const [genres, setGenres] = useState([])

  // Modal state
  const [modal, setModal] = useState(false)

  // Get All Movies data from API
  useEffect(() => {
    getAllTMDB().then(res => setMovies(res))
  }, [])

  // Gets single movie data and open modal
  const handleClick = async e => {
    let movieData = await getTMDB(e.target.id)
    const directors = []
    movieData?.credits?.crew?.forEach(
      member =>
        member.known_for_department === 'Directing' &&
        directors.push(member.name)
    )
    movieData = { ...movieData, directors }
    setSingleMovie(movieData)
    let genresIf = []
    let obj
    movies?.forEach(genre => {
      obj = {
        name: genre.title,
        color: genre.color,
      }
      genresIf.push(obj)
    })
    if ((genresIf.length = 10)) {
      setGenres(genresIf)

      setModal(!modal)
    }
  }

  const modalIsOpen = () => {
    setModal(!modal)
  }

  return (
    <Movies>
      <SearchInput />
      {movies?.map((movie, i) => (
        <MovieRow
          movies={movie.movies.data.results}
          title={movie.title}
          color={movie.color}
          handleClick={handleClick}
          key={i}
        />
      ))}
      {modal && (
        <>
          <MovieModal
            modal={modal}
            modalIsOpen={modalIsOpen}
            movie={singleMovie}
            genresColors={genres}
          />
        </>
      )}
    </Movies>
  )
}

export default MoviesContainer

const Movies = styled(Container)`
  text-align: center;
  max-width: 3000px;

  @media screen and (max-width: 1500px) {
    min-width: 500px;
  }
`
