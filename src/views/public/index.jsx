import React from 'react'
import { Router } from '@reach/router'
import Movies from './Movies'
import Login from './Login'
import Join from './Join'

const Public = () => (
  <Router>
    <Movies path='/movies' />
    <Login path='/login' />
    <Join path='/join' />
  </Router>
)

export default Public
