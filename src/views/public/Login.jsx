import React, { useState } from 'react'
import Login from '../../components/LoginForm'

const LoginContainer = () => {
  const [isPasswordVisible, setIsPasswordVisible] = useState(false)

  const handleClick = () => {
    setIsPasswordVisible(!isPasswordVisible)
  }

  return (
    <>
      <Login handleClick={handleClick} isPasswordVisible={isPasswordVisible} />
    </>
  )
}

export default LoginContainer
