import { http } from '../config/http'
import axios from 'axios'

const defaultPath = '/movies'
const baseURL = 'https://api.themoviedb.org/3'
const API_KEY = '479b26e5222f9ef3fac0b4d50717c56b'

var movieData = []
export const getMovies = () => http.get(defaultPath)

const basicRequest = (endpoint) => axios.get(baseURL + endpoint)

// Fake movie data
export const getTMDB = async id => {
  await axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}&append_to_response=credits,videos`)
    .then((res) => {
      movieData = res.data
    })
    .catch(err => console.error(err))
  return movieData
}

export const getAllTMDB = async () => {
  return [
    {
      slug: 'trending',
      title: 'Trending',
      color: '#000',
      movies: await basicRequest(`/trending/all/week?api_key=${API_KEY}`)
    }, {
      slug: 'toprated',
      title: 'Top Rated',
      color: '#32a',
      movies: await basicRequest(`/movie/top_rated?api_key=${API_KEY}`)
    },
    {
      slug: 'action',
      title: 'Action',
      color: '#CC5146',
      movies: await basicRequest(`/discover/movie?with_genres=28&region=US&api_key=${API_KEY}`)
    }, {
      slug: 'drama',
      title: 'Drama',
      color: '#5238D9',
      movies: await basicRequest(`/discover/movie?with_genres=18&region=US&api_key=${API_KEY}`)
    }, {
      slug: 'animation',
      title: 'Animation',
      color: '#70AF53',
      movies: await basicRequest(`/discover/movie?with_genres=16&region=US&api_key=${API_KEY}`)
    },
    {
      slug: 'comedy',
      title: 'Comedy',
      color: '#F9A139',
      movies: await basicRequest(`/discover/movie?with_genres=35&region=US&api_key=${API_KEY}`)
    },
    {
      slug: 'documentary',
      title: 'Documentary',
      color: 'brown',
      movies: await basicRequest(`/discover/movie?with_genres=99&region=US&api_key=${API_KEY}`)
    }, {
      slug: 'horror',
      title: 'Horror',
      color: 'black',
      movies: await basicRequest(`/discover/movie?with_genres=27&region=US&api_key=${API_KEY}`)
    }, {
      slug: 'music',
      title: 'Music',
      color: 'orange',
      movies: await basicRequest(`/discover/movie?with_genres=10402&region=US&api_key=${API_KEY}`)
    },
    {
      slug: 'romance',
      title: 'Romance',
      color: 'red',
      movies: await basicRequest(`/discover/movie?with_genres=10749&region=US&api_key=${API_KEY}`)
    }
  ]

}