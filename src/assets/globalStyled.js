import { createGlobalStyle } from 'styled-components'
import '../fonts/Blockletter.otf'

const GlobalStyle = createGlobalStyle`

* {
    margin: 0;
    padding: 0;
    outline: none;
    box-shadow: border-box;
}

ul {
    font-family: 'Blockletter', sans-serif;
}

body {
    font-family: 'montserrat', sans-serif;
    background-color: ${props => props.theme.bgColor};
}

body, #root {
    height: 100vh;
}

`


export default GlobalStyle