export const mainTheme = {
  primary: '#00367d',
  support: '#00204a',
  pastel: '#217093',
  body: '#000a17',
  bgColor: '#b7d6eb',
  fontColor: '#fff',
  yellow: '#facf39',
  supportYellow: '#fbdb6b'
}