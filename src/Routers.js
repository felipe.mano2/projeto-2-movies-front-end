import React from 'react'
import { Router } from '@reach/router'
import Public from './views/public'
import Layout from './components/Layout'
import User from './views/user/'

const Routers = () => (
    <Layout>
        <Router>
            <Public path='/*' />
            <User path='/user/*' />
        </Router>
    </Layout>
)


export default Routers